package models.commands;

import interfaces.Command;
import models.Home;

public class TurnLightingOffCommand implements Command {

    private Home home;

    public TurnLightingOffCommand(Home home) {
        this.home = home;
    }

    @Override
    public void execute() {
        home.setLightingOn(false);
    }

    @Override
    public void unexecute() {
        home.setLightingOn(true);
    }
}
