package models.commands;

        import interfaces.Command;
        import models.Home;

public class TurnHeatingOnCommand implements Command {

    private Home home;

    public TurnHeatingOnCommand(Home home) {
        this.home = home;
    }

    @Override
    public void execute() {
        home.setHeatingOn(true);
    }

    @Override
    public void unexecute() {
        home.setHeatingOn(false);
    }
}
