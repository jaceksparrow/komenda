package models.commands;

import interfaces.Command;
import models.Home;

public class TurnHeatingOffCommand implements Command {

    private Home home;

    public TurnHeatingOffCommand(Home home) {
        this.home = home;
    }

    @Override
    public void execute() {
        home.setHeatingOn(false);
    }

    @Override
    public void unexecute() {
        home.setHeatingOn(true);
    }
}
