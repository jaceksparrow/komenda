package models.commands;

import interfaces.Command;
import models.Home;

public class TurnCoolingOffCommand implements Command {

    private Home home;

    public TurnCoolingOffCommand(Home home) {
        this.home = home;
    }

    @Override
    public void execute() {
        home.setCoolingOn(false);
    }

    @Override
    public void unexecute() {
        home.setCoolingOn(true);
    }
}
