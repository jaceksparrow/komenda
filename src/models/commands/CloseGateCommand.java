package models.commands;

import interfaces.Command;
import models.Home;

public class CloseGateCommand implements Command {

    private Home home;

    public CloseGateCommand(Home home) {
        this.home = home;
    }

    @Override
    public void execute() {
        home.setGateOpened(false);
    }

    @Override
    public void unexecute() {
        home.setGateOpened(true);
    }
}
