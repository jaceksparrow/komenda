package models.commands;

import interfaces.Command;
import models.Home;

public class TurnLightingOnCommand implements Command {

    private Home home;

    public TurnLightingOnCommand(Home home) {
        this.home = home;
    }

    @Override
    public void execute() {
        home.setLightingOn(true);
    }

    @Override
    public void unexecute() {
        home.setLightingOn(false);
    }
}
