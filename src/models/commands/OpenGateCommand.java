package models.commands;

import interfaces.Command;
import models.Home;

public class OpenGateCommand implements Command {

    private Home home;

    public OpenGateCommand(Home home) {
        this.home = home;
    }

    @Override
    public void execute() {
        home.setGateOpened(true);
    }

    @Override
    public void unexecute() {
        home.setGateOpened(false);
    }
}
