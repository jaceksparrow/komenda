package models.commands;

import interfaces.Command;
import models.Home;

public class TurnMusicVolumeUpCommand implements Command {

    private Home home;

    public TurnMusicVolumeUpCommand(Home home) {
        this.home = home;
    }

    @Override
    public void execute() {
        home.turnMusicVolumeUp();
    }

    @Override
    public void unexecute() {
        home.turnMusicVolumeDown();
    }
}
