package models.commands;

import interfaces.Command;
import models.Home;

public class TurnMusicVolumeDownCommand implements Command {

    private Home home;

    public TurnMusicVolumeDownCommand(Home home) {
        this.home = home;
    }

    @Override
    public void execute() {
        home.turnMusicVolumeDown();
    }

    @Override
    public void unexecute() {
        home.turnMusicVolumeUp();
    }
}
