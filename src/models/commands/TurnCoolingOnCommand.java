package models.commands;

import interfaces.Command;
import models.Home;

public class TurnCoolingOnCommand implements Command {

    private Home home;

    public TurnCoolingOnCommand(Home home) {
        this.home = home;
    }

    @Override
    public void execute() {
        home.setCoolingOn(true);
    }

    @Override
    public void unexecute() {
        home.setCoolingOn(false);
    }
}
