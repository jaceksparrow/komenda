package models.remotes;

import models.Home;
import models.commands.TurnCoolingOffCommand;
import models.commands.TurnCoolingOnCommand;
import models.commands.TurnHeatingOffCommand;
import models.commands.TurnHeatingOnCommand;

public class HeatingRemote extends Remote {

    public HeatingRemote(Home home) {
        super(home, "Ogrzewanie", new TurnHeatingOnCommand(home), new TurnHeatingOffCommand(home));
    }
}
