package models.remotes;

import models.Home;
import models.commands.TurnCoolingOffCommand;
import models.commands.TurnCoolingOnCommand;
import models.commands.TurnLightingOffCommand;
import models.commands.TurnLightingOnCommand;

public class LightingRemote extends Remote {

    public LightingRemote(Home home) {
        super(home, "Oświetlenie", new TurnLightingOnCommand(home), new TurnLightingOffCommand(home));
    }
}
