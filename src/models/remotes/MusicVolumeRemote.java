package models.remotes;

import models.Home;
import models.commands.TurnHeatingOffCommand;
import models.commands.TurnHeatingOnCommand;
import models.commands.TurnMusicVolumeDownCommand;
import models.commands.TurnMusicVolumeUpCommand;

public class MusicVolumeRemote extends Remote {

    public MusicVolumeRemote(Home home) {
        super(home, "Muzyka", new TurnMusicVolumeUpCommand(home), new TurnMusicVolumeDownCommand(home));
    }
}
