package models.remotes;

import models.Home;
import models.commands.CloseGateCommand;
import models.commands.OpenGateCommand;
import models.commands.TurnCoolingOffCommand;
import models.commands.TurnCoolingOnCommand;

public class CoolingRemote extends Remote {

    public CoolingRemote(Home home) {
        super(home, "Klimatyzacja", new TurnCoolingOnCommand(home), new TurnCoolingOffCommand(home));
    }
}
