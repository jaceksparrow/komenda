package models.remotes;

import models.Home;
import models.commands.CloseGateCommand;
import models.commands.OpenGateCommand;

public class GateRemote extends Remote {

    public GateRemote(Home home) {
        super(home, "Brama", new OpenGateCommand(home), new CloseGateCommand(home));
    }
}
