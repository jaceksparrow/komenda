package models.remotes;

import interfaces.Command;
import models.Home;

public abstract class Remote {
    private Home home;
    private final String name;
    private Command onCommand;
    private Command offCommand;

    public Remote(Home home, String name, Command onCommand, Command offCommand) {
        this.home = home;
        this.name = name;
        this.onCommand = onCommand;
        this.offCommand = offCommand;
    }

    public Home getHome() {
        return home;
    }

    public void setHome(Home home) {
        this.home = home;
    }

    public String getName() {
        return name;
    }

    public Command getOnCommand() {
        return onCommand;
    }

    public void setOnCommand(Command onCommand) {
        this.onCommand = onCommand;
    }

    public Command getOffCommand() {
        return offCommand;
    }

    public void setOffCommand(Command offCommand) {
        this.offCommand = offCommand;
    }
}
