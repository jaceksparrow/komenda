package models;

/**
 * Our intelligent home
 */
public class Home {

    private boolean isHeatingOn;
    private boolean isCoolingOn;
    private boolean isLightingOn;
    private boolean isGateOpened;
    private int musicVolume;

    public Home() {

    }

    public Home(boolean isHeatingOn, boolean isCoolingOn, boolean isLightingOn, boolean isGateOpened, int musicVolume) {
        this.isHeatingOn = isHeatingOn;
        this.isCoolingOn = isCoolingOn;
        this.isLightingOn = isLightingOn;
        this.isGateOpened = isGateOpened;
        this.musicVolume = musicVolume;
    }

    public boolean isHeatingOn() {
        return isHeatingOn;
    }

    public void setHeatingOn(boolean heatingOn) {
        isHeatingOn = heatingOn;
    }

    public boolean isCoolingOn() {
        return isCoolingOn;
    }

    public void setCoolingOn(boolean coolingOn) {
        isCoolingOn = coolingOn;
    }

    public boolean isLightingOn() {
        return isLightingOn;
    }

    public void setLightingOn(boolean lightingOn) {
        isLightingOn = lightingOn;
    }

    public boolean isGateOpened() {
        return isGateOpened;
    }

    public void setGateOpened(boolean gateOpened) {
        isGateOpened = gateOpened;
    }

    public int getMusicVolume() {
        return musicVolume;
    }

    public void setMusicVolume(int musicVolume) {
        this.musicVolume = musicVolume;
    }

    public void turnMusicVolumeUp() {
        if (musicVolume < 10) {
            musicVolume++;
        }
    }

    public void turnMusicVolumeDown() {
        if (musicVolume > 0) {
            musicVolume--;
        }
    }
}
