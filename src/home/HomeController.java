package home;

import interfaces.Command;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import models.Home;
import models.remotes.*;

import java.util.ArrayList;

public class HomeController {

    private static final String ON = "wł.";
    private static final String OFF = "wył.";
    private static final String OPENED = "otw.";
    private static final String CLOSED = "zamk.";

    private Home home;
    private ArrayList<Remote> remotes;
    private ArrayList<String> remoteNames;
    private ArrayList<Command> commands;

    @FXML
    private ChoiceBox choiceBox0;
    @FXML
    private ChoiceBox choiceBox1;
    @FXML
    private ChoiceBox choiceBox2;
    @FXML
    private ChoiceBox choiceBox3;
    @FXML
    private ChoiceBox choiceBox4;
    @FXML
    private Label heatingLabel;
    @FXML
    private Label coolingLabel;
    @FXML
    private Label lightingLabel;
    @FXML
    private Label musicLabel;
    @FXML
    private Label gateLabel;

    public HomeController() {
        home = new Home();
        remotes = new ArrayList<>();
        remoteNames = new ArrayList<>();
        commands = new ArrayList<>();

        initializeRemotes();
    }

    @SuppressWarnings("unchecked")
    @FXML
    public void initialize() {
        choiceBox0.setItems(FXCollections.observableArrayList(remoteNames));
        choiceBox0.getSelectionModel().selectFirst();

        choiceBox1.setItems(FXCollections.observableArrayList(remoteNames));
        choiceBox1.getSelectionModel().select(1);

        choiceBox2.setItems(FXCollections.observableArrayList(remoteNames));
        choiceBox2.getSelectionModel().select(2);

        choiceBox3.setItems(FXCollections.observableArrayList(remoteNames));
        choiceBox3.getSelectionModel().select(3);

        choiceBox4.setItems(FXCollections.observableArrayList(remoteNames));
        choiceBox4.getSelectionModel().select(4);

        updateHomePanel();
    }

    private void initializeRemotes() {
        remotes.add(new HeatingRemote(home));
        remotes.add(new CoolingRemote(home));
        remotes.add(new LightingRemote(home));
        remotes.add(new GateRemote(home));
        remotes.add(new MusicVolumeRemote(home));

        for (Remote remote : remotes) {
            remoteNames.add(remote.getName());
        }
    }


    private void updateHomePanel() {
        heatingLabel.setText(home.isHeatingOn() ? ON : OFF);
        coolingLabel.setText(home.isCoolingOn() ? ON : OFF);
        lightingLabel.setText(home.isLightingOn() ? ON : OFF);
        gateLabel.setText(home.isGateOpened() ? OPENED : CLOSED);
        musicLabel.setText(String.valueOf(home.getMusicVolume()));

    }

    private void handleOnButtonClick(Remote remote) {
        remote.getOnCommand().execute();
        commands.add(remote.getOnCommand());
        updateHomePanel();

        System.out.println(commands.size());
    }

    private void handleOffButtonClick(Remote remote) {
        remote.getOffCommand().execute();
        commands.add(remote.getOffCommand());
        updateHomePanel();

        System.out.println(commands.size());
    }

    public void onCancelButtonClick() {
        if (!commands.isEmpty()) {
            int index = commands.size() - 1;
            commands.get(index).unexecute();
            commands.remove(index);
            updateHomePanel();
        }
    }

    public void onOnButton0Click() {
        handleOnButtonClick(remotes.get(choiceBox0.getSelectionModel().getSelectedIndex()));
    }

    public void onOnButton1Click() {
        handleOnButtonClick(remotes.get(choiceBox1.getSelectionModel().getSelectedIndex()));
    }

    public void onOnButton2Click() {
        handleOnButtonClick(remotes.get(choiceBox2.getSelectionModel().getSelectedIndex()));
    }

    public void onOnButton3Click() {
        handleOnButtonClick(remotes.get(choiceBox3.getSelectionModel().getSelectedIndex()));
    }

    public void onOnButton4Click() {
        handleOnButtonClick(remotes.get(choiceBox4.getSelectionModel().getSelectedIndex()));
    }

    public void onOffButton0Click() {
        handleOffButtonClick(remotes.get(choiceBox0.getSelectionModel().getSelectedIndex()));
    }

    public void onOffButton1Click() {
        handleOffButtonClick(remotes.get(choiceBox1.getSelectionModel().getSelectedIndex()));
    }

    public void onOffButton2Click() {
        handleOffButtonClick(remotes.get(choiceBox2.getSelectionModel().getSelectedIndex()));
    }

    public void onOffButton3Click() {
        handleOffButtonClick(remotes.get(choiceBox3.getSelectionModel().getSelectedIndex()));
    }

    public void onOffButton4Click() {
        handleOffButtonClick(remotes.get(choiceBox4.getSelectionModel().getSelectedIndex()));
    }
}
